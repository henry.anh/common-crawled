import logging
from selenium.webdriver.chrome.options import Options as ChromeOptions

from core.worker import Worker
from core.message_queue import MessageQueue
from utils.fake_module import ScrapeOpsFakeUserAgentMiddleware
from core.base import BaseLinkGenerator, BaseDriverContainer, InfoApiSetting, BaseContentSaver, BasePageParser
from core.controller import WebCrawlingController

from domains.bank.container import DriverContainer
from domains.bank.generator import LinkGenerator
from domains.bank.parser import Parser
from domains.bank.saver import ContentSaver


if __name__ == "__main__":
    MAX_WORKERS = 5
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s | %(name)s | [%(levelname)s] | %(message)s",
        handlers=[logging.StreamHandler()]
    )
    message_queue = MessageQueue()
    options = ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--igconito")
    options.add_argument("--disable-extensions")

    base_url = ""
    save_file = r"storages/bank/bank.jsonl"
    link_save_file = r"storages/bank/bank.txt"
    force_new = True
    link_generator = LinkGenerator(
        base_url=base_url,
        container=DriverContainer(
            options=options,
            fake_agents=ScrapeOpsFakeUserAgentMiddleware(settings=InfoApiSetting())
        ),
        message_queue=message_queue,
        save_file=link_save_file
    )
    saver = ContentSaver(save_file=save_file, force_new=force_new)

    workers = [
        Worker(
            thread_id=idx,
            message_queue=message_queue,
            page_parser=Parser(container=DriverContainer(
                options=options,
                fake_agents=ScrapeOpsFakeUserAgentMiddleware(settings=InfoApiSetting()),
            ))
        )
        for idx in range(MAX_WORKERS)
    ]
    controller = WebCrawlingController(
        message_queue=message_queue,
        workers=workers, saver=saver, link_generator=link_generator
    )
    controller.run()
