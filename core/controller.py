import logging
from typing import List
from threading import Event, Thread
from queue import Empty

from core.message_queue import MessageQueue
from core.worker import Worker
from core.base import BaseContentSaver
from core.base import BaseLinkGenerator


class WebCrawlingController:

    def __init__(self,
                 message_queue: MessageQueue,
                 workers: List[Worker],
                 saver: BaseContentSaver,
                 link_generator: BaseLinkGenerator
                 ):
        self.workers = workers
        self.saver = saver
        self.link_generator = link_generator
        self.message_queue = message_queue

        self.stop = Event()
        self.stop.clear()
        self.timer = Event()
        self.logger = logging.getLogger(self.__class__.__name__)

    def save_contents(self):
        while not self.stop.is_set():
            try:
                content = self.message_queue.contents.get(timeout=10)
                self.saver.run(content=content)
            except Empty:
                self.logger.info(f"No content found")

    def run(self):
        for worker in self.workers:
            worker.start()

        save_thread = Thread(target=self.save_contents, daemon=True)
        save_thread.start()

        self.link_generator.run()

        while True:
            self.timer.wait(timeout=60)
            if self.message_queue.urls.empty():
                self.stop.set()
                for worker in self.workers:
                    worker.finish()
                break

        for worker in self.workers:
            worker.join()

        save_thread.join()


