import logging
from threading import Thread, Event

from common.constants import *
from core.base import BasePageParser
from core.message_queue import MessageQueue, Empty


class Worker(Thread):

    def __init__(self,
                 thread_id: int,
                 message_queue: MessageQueue,
                 page_parser: BasePageParser
                 ):
        super(Worker, self).__init__()
        self.message_queue = message_queue
        self.page_parser = page_parser
        self.logger = logging.getLogger(self.__class__.__name__ + f"-{thread_id}")
        self.stop = Event()
        self.clock = Event()

    def finish(self):
        self.stop.set()

    def run(self):
        while not self.stop.is_set():
            try:
                url = self.message_queue.urls.get(timeout=5)
            except Empty:
                url = None
                self.logger.info("No URL found")

            if url is not None:
                try:
                    content = self.page_parser.parse_url(url=url)
                    self.clock.wait(timeout=WORKER_WAIT_TIME)
                except Exception as e:
                    self.logger.error(e)
                    content = None

                if content is not None:
                    self.message_queue.contents.put(content)
