from queue import Queue, Empty


class MessageQueue:

    def __init__(self, max_urls: int = 0, max_content: int = 0):
        self.urls = Queue(maxsize=max_urls)
        self.contents = Queue(maxsize=max_content)
