import logging
import time
from bs4 import BeautifulSoup
from lxml import etree
from abc import ABC, abstractmethod
from typing import Optional, Dict, Any
from selenium.webdriver.common.options import ArgOptions
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import selenium.webdriver.support.expected_conditions as ec

from utils.fake_module import ScrapeOpsFakeUserAgentMiddleware, InfoApiSetting
from core.message_queue import MessageQueue


class BaseDriverContainer(ABC):

    def __init__(self,
                 options: ArgOptions,
                 fake_agents: Optional[ScrapeOpsFakeUserAgentMiddleware] = None,
                 max_accessed_urls: int = 100
                 ):
        super(BaseDriverContainer, self).__init__()

        self.options = options
        self.fake_agents = fake_agents if fake_agents is not None else ScrapeOpsFakeUserAgentMiddleware(
            settings=InfoApiSetting()
        )
        self.__driver = self.init_driver()
        self.max_accessed_urls = max_accessed_urls
        self.current_accessed_urls = 0
        self.logger = logging.getLogger(self.__class__.__name__)

    def force_init(self):
        self.__driver.close()
        self.__driver.quit()
        self.__driver = self.init_driver()
        time.sleep(4)

    @abstractmethod
    def init_driver(self) -> WebDriver:
        raise NotImplementedError("Need to be implemented")

    @property
    def driver(self) -> WebDriver:
        if self.current_accessed_urls == self.max_accessed_urls:
            self.force_init()
            self.current_accessed_urls = 0
            self.logger.info(f"Re-initialize driver")
        self.current_accessed_urls += 1
        return self.__driver


class BasePageParser(ABC):

    def __init__(self, container: BaseDriverContainer):
        self.container = container
        self.logger = logging.getLogger(self.__class__.__name__)

    # --------------------- SELENIUM --------------------- #
    def find_elements_by_xpath(self, xpath: str):
        return self.container.driver.find_elements(by=By.XPATH, value=xpath)

    def find_element_by_xpath(self, xpath: str):
        return self.container.driver.find_element(by=By.XPATH, value=xpath)

    # --------------------- BEAUTIFULSOUP --------------------- #

    def get_html_soup(self) -> BeautifulSoup:
        return BeautifulSoup(
            self.container.driver.page_source,
            "lxml"
        )

    def find_element_by_etree(self, xpath: str):
        soup = BeautifulSoup(self.container.driver.page_source, "html.parser")
        dom = etree.HTML(str(soup))

        return dom.xpath(xpath)

    def find_all_elements_by_tag_bs(self, tag_name: str, soup: Optional[BeautifulSoup],
                                    attrs: Optional[Dict[str, Any]] = None
                                    ):
        if soup is None:
            _soup = self.get_html_soup()
        else:
            _soup = soup
        if attrs is not None:
            return _soup.find_all(name=tag_name, attrs=attrs)
        else:
            return _soup.find_all(name=tag_name)

    def find_element_by_tag_bs(self, tag_name: str, soup: Optional[BeautifulSoup],
                               attrs: Optional[Dict[str, Any]] = None
                               ):
        if soup is None:
            _soup = self.get_html_soup()
        else:
            _soup = soup
        if attrs is not None:
            return _soup.find(name=tag_name, attrs=attrs)
        else:
            return _soup.find(name=tag_name)

    @abstractmethod
    def parse_url(self, url: str) -> Dict[str, Any]:
        raise NotImplementedError("Need to be implemented")


class BaseLinkGenerator(ABC):

    def __init__(self, base_url: str,
                 container: BaseDriverContainer,
                 message_queue: MessageQueue,
                 crawled_links: set = None
                 ):
        super(BaseLinkGenerator, self).__init__()
        self.base_url = base_url
        self.container = container
        self.message_queue = message_queue
        self.crawled_links = crawled_links if crawled_links is not None else set()
        self.logger = logging.getLogger(self.__class__.__name__)

    def is_crawled(self, url: str) -> bool:
        return url in self.crawled_links

    @abstractmethod
    def run(self):
        raise NotImplementedError("Need to be implemented")


class BaseContentSaver(ABC):

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    @abstractmethod
    def run(self, content: Any):
        raise NotImplementedError("Need to be implemented")


class BaseOperator(ABC):

    def __init__(self, container: BaseDriverContainer):
        super(BaseOperator, self).__init__()

        self.container = container
        self.logger = logging.getLogger(self.__class__.__name__)
        self.height = 0

    def scroll_page(self, height: int = None):
        if height is not None:
            javascript = f"window:scrollTo({self.height}, {self.height + height});"
            self.height += height
        else:
            javascript = f"window:scrollTo({self.height}, document.body.scrollHeight);"
        self.container.driver.execute_script(javascript)

    def wait_driver_load_element(self, wait_time, xpath):
        return WebDriverWait(
            self.container.driver, timeout=wait_time
        ).until(ec.presence_of_element_located((By.XPATH, xpath)))

    def click_button(self, xpath):
        button = self.container.driver.find_element(by=By.XPATH, value=xpath)
        if button is not None:
            try:
                button.click()
            except Exception as e:
                self.logger.info(f"Can't click button!!! Having error {e}")

    @abstractmethod
    def process(self):
        raise NotImplementedError("Need to be implemented")









