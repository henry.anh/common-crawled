import requests
from random import randint
from typing import Optional
from bs4 import BeautifulSoup
from selenium import webdriver
from urllib.parse import urlencode
from selenium.webdriver.firefox.options import Options as FirefoxOptions


class InfoApiSetting:
    SCRAPEOPS_API_KEY = "43e4ca2e-e9b1-4349-a5e9-897d9f5c1efd"
    SCRAPEOPS_FAKE_USER_AGENT_ENDPOINT = "http://headers.scrapeops.io/v1/user-agents?"
    SCRAPEOPS_FAKE_BROWSER_HEADER_ENDPOINT = 'http://headers.scrapeops.io/v1/browser-headers?'
    SCRAPEOPS_FAKE_USER_AGENT_ENABLED = True
    SCRAPEOPS_NUM_RESULTS = 10


class ScrapeOpsFakeUserAgentMiddleware:

    def __init__(self, settings: InfoApiSetting, log_path: Optional[str] = None):
        self.scrapeops_api_key = settings.SCRAPEOPS_API_KEY
        self.scrapeops_endpoint = settings.SCRAPEOPS_FAKE_USER_AGENT_ENDPOINT
        self.scrapeops_fake_user_agents_active = settings.SCRAPEOPS_FAKE_USER_AGENT_ENABLED
        self.scrapeops_num_results = settings.SCRAPEOPS_NUM_RESULTS
        self.fake_browser_header_endpoint = settings.SCRAPEOPS_FAKE_BROWSER_HEADER_ENDPOINT
        self.headers_list = []
        self.log_path = log_path
        self._get_user_agents_list()
        self._scrapeops_fake_user_agents_enabled()

    def _get_user_agents_list(self):
        payload = {'api_key': self.scrapeops_api_key}
        if self.scrapeops_num_results is not None:
            payload['num_results'] = self.scrapeops_num_results
        response = requests.get(self.scrapeops_endpoint, params=urlencode(payload))
        json_response = response.json()
        self.user_agents_list = json_response.get('result', [])

    def _get_browser_header_list(self):
        payload = {"api_key": self.scrapeops_api_key}
        if self.scrapeops_num_results is not None:
            payload['num_results'] = self.scrapeops_num_results
        response = requests.get(self.fake_browser_header_endpoint, params=urlencode(payload))
        json_response = response.json()
        self.headers_list = json_response.get('result', [])

    def _get_random_user_agent(self):
        random_index = randint(0, len(self.user_agents_list) - 1)
        return self.user_agents_list[random_index]

    def _get_random_header(self):
        random_idx = randint(0, len(self.headers_list) - 1)
        return self.headers_list[random_idx]

    def get_fake_header(self):
        fake_header = self._get_random_header()
        return fake_header

    def _scrapeops_fake_user_agents_enabled(self):
        if self.scrapeops_api_key is None or self.scrapeops_api_key == '' or not self.scrapeops_fake_user_agents_active:
            self.scrapeops_fake_user_agents_active = False
        else:
            self.scrapeops_fake_user_agents_active = True

    def get_fake_agent(self):
        random_user_agent = self._get_random_user_agent()
        return random_user_agent

    def setup_selenium_firefox(self, headless: bool = True, fake_agent: bool = True):
        service = webdriver.FirefoxService()
        firefox_options = FirefoxOptions()
        firefox_options.set_preference('devtools.jsonview.enabled', False)
        firefox_options.set_preference('dom.webnotifications.enabled', False)
        firefox_options.add_argument("--test-type")
        firefox_options.add_argument('--ignore-certificate-errors')
        firefox_options.add_argument('--disable-extensions')
        firefox_options.add_argument('disable-infobars')
        firefox_options.add_argument("--incognito")
        if fake_agent:
            agent = self.get_fake_agent()
            firefox_options.set_preference("general.useragent.override", agent)
        if headless:
            firefox_options.add_argument("--headless")
        driver = webdriver.Firefox(service=service, options=firefox_options)
        return driver

    def access_website_driver(self, url, headless):
        driver = self.setup_selenium_firefox(headless=headless)
        res = ""
        for _ in range(5):
            try:
                res = ""
                driver.get(url)
                break
            except Exception as e:
                res = None
                continue
        if res is None:
            driver.close()
            return None

        return driver

    def access_website(self, url, headless: bool = True):
        driver = self.setup_selenium_firefox(headless)
        res = ""
        for _ in range(5):
            try:
                res = ""
                driver.get(url)
                break
            except Exception as e:
                res = None
                continue
        if res is None:
            driver.close()
            return None
        res = driver.page_source
        if res is not None:
            driver.close()
            return BeautifulSoup(res, "lxml")
        else:
            driver.close()
            return None
