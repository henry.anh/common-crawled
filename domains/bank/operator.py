from core.base import BaseDriverContainer, BaseOperator


class CustomOperator(BaseOperator):

    def __init__(self, container: BaseDriverContainer) -> None:
        super().__init__(container)

    def process(self):
        time.sleep(1)
        self.click_button(path="//*[@id=\"btn_submit\"]")
        print("CLICK Success")
        time.sleep(1)
        for _ in range(5):
            print("scroll")
            self.scroll_page(200)
            time.sleep(1)


if __name__ == "__main__":
    import time
    from domains.vinmec.container import DriverContainer
    from pprint import pprint
    from selenium.webdriver.chrome.options import Options as ChromeOptions

    from domains.thebank.container import DriverContainer
    from domains.thebank.parser import Parser
    from utils.fake_module import ScrapeOpsFakeUserAgentMiddleware, InfoApiSetting

    options = ChromeOptions()
    # options.add_argument("--headless")
    options.add_argument("--igcognito")
    options.add_argument("--disable-extensions")

    fake_agent = ScrapeOpsFakeUserAgentMiddleware(settings=InfoApiSetting())
    container = DriverContainer(options=options, fake_agent=fake_agent)
    parser = Parser(container=container)

    url = "https://thebank.vn/cong-cu/tim-chi-nhanh-ngan-hang/sacombank-tan-binh-tp-hcm-6-33-76.html"

    st = time.perf_counter()
    # pprint(parser.parse_url("https://thebank.vn/cong-cu/tim-atm/bidv-quan-10-tp-hcm-41-33-50.html"))
    # pprint(parser.parse_url(url))
    pprint(parser.parse_url_by_soup(url))
    print(f">>> DONE: {round(time.perf_counter() - st, 5)}s")
