from typing import Any, Dict
from core.base import BasePageParser, BaseDriverContainer
from domains.bank.operator import CustomOperator


class Parser(BasePageParser):
    def __init__(self, container: BaseDriverContainer) -> None:
        super().__init__(container)
        self.operator = CustomOperator(container)

    def parse_url(self, url: str) -> Dict[str, Any]:
        self.container.driver.get(url)
        output = {"url": url}
        try:
            name_bank = self.find_elements_by_xpath(
                xpath="//*[@id=\"company_chosen\"]/a/span"
            )[0].get_attribute("textContent")

            province = self.find_elements_by_xpath(
                xpath="//*[@id=\"province_chosen\"]/a/span"
            )[0].get_attribute("textContent")

            district = self.find_elements_by_xpath(
                xpath="//*[@id=\"district_chosen\"]/a/span"
            )[0].get_attribute("textContent")

            output.update({
                "name_bank": name_bank,
                "province": province,
                "district": district
            })
            address_table = self.find_elements_by_xpath(
                xpath="/html/body/main/div[4]/div[4]/div[1]/div/div[1]/table/tbody/tr/td[3]"
            )
            name_table = self.find_elements_by_xpath(
                xpath="/html/body/main/div[4]/div[4]/div[1]/div/div[1]/table/tbody/tr/td[2]"
            )
            info = []
            for name, address in zip(name_table, address_table):
                info.append({"name": name.text, "address": address.text})
            output.update(
                {"info": info}
            )
            self.logger.info(f"Successfully parsed {url}")
        except Exception as e:
            self.logger.exception(f"Failed to parse link {url}\n{e}")
        return output

    def parse_url_by_soup(self, url: str) -> Dict[str, Any]:
        self.container.driver.get(url)
        output = {"url": url}
        # self.operator.process()
        soup = self.get_html_soup()

        if soup is None:
            return output

        try:
            info = [ele.text for ele in
                    self.find_all_elements_by_tag_bs(soup=soup, tag_name="a", attrs={"class": "chosen-single"})]

            info_address = []
            for ele in self.find_all_elements_by_tag_bs(soup=soup, tag_name="td", attrs={"class": "td-content-mobile"}):
                name = self.find_element_by_tag_bs(soup=ele, tag_name="p", attrs={"class": "brad"}).text.strip()
                # name = ele.find("p", class_="brad").text.strip()

                address = [e.strip() for e in
                           self.find_element_by_tag_bs(soup=ele,
                                                       tag_name="p",
                                                       attrs={"class": "text_mobile"}).text.split("\n")
                           if e != ""][0]
                info_address.append({"name": name, "address": address})

            output.update({
                "name_bank": info[0],
                "province": info[1],
                "district": info[2],
                "info": info_address
            })
            self.logger.info(f"Successfully parsed {url}")
        except Exception as e:
            self.logger.exception(f"Failed to parse link {url}\n{e}")

        return output


if __name__ == "__main__":
    import time
    from domains.thebank.container import DriverContainer
    from pprint import pprint
    from selenium.webdriver.chrome.options import Options as ChromeOptions
    from utils.fake_module import ScrapeOpsFakeUserAgentMiddleware, InfoApiSetting

    options = ChromeOptions()
    # options.add_argument("--headless")
    options.add_argument("--igcognito")
    options.add_argument("--disable-extensions")

    container = DriverContainer(options=options,
                                fake_agent=ScrapeOpsFakeUserAgentMiddleware(settings=InfoApiSetting()))

    parser = Parser(container=container)

    url = "https://thebank.vn/cong-cu/tim-chi-nhanh-ngan-hang/sacombank-tan-binh-tp-hcm-6-33-76.html"

    st = time.perf_counter()
    # pprint(parser.parse_url("https://thebank.vn/cong-cu/tim-atm/bidv-quan-10-tp-hcm-41-33-50.html"))
    # pprint(parser.parse_url(url))
    pprint(parser.parse_url_by_soup(url))
    print(f">>> DONE: {round(time.perf_counter() - st, 5)}s")
