import time
from time import sleep
from unidecode import unidecode
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor

from selenium.webdriver.common.by import By

from core.base import BaseDriverContainer, BaseLinkGenerator
from core.message_queue import MessageQueue
from utils.fake_module import ScrapeOpsFakeUserAgentMiddleware, InfoApiSetting


class LinkGenerator(BaseLinkGenerator):
    def __init__(
            self,
            base_url: str,
            container: BaseDriverContainer,
            message_queue: MessageQueue,
            save_file: str,
            crawled_links: set = None,
    ) -> None:
        super().__init__(base_url, container, message_queue, crawled_links)
        self.save_file = save_file
        self.base_url = base_url
        self.api_setting = InfoApiSetting()
        self.browser = ScrapeOpsFakeUserAgentMiddleware(
            self.api_setting,
            log_path=r"domains/bank/logs/crawler_bank.log"
        )
        self.link_queue = []

    def get_link_of_bank(self, _url):
        list_url = []
        banks_soup = self.browser.access_website(_url, True)
        banks_soup = banks_soup.find("select", class_="select_company sl_chosen chosen_js")
        banks = banks_soup.find_all("option")

        for t in banks:
            bank_idx = t.get("value")
            bank_name = t.text

            encode_bank_name = "-".join([name.lower() for name in bank_name.split()])
            bank_with_idx = "-".join([encode_bank_name, bank_idx])
            url = self.base_url + bank_with_idx + ".html"
            sample = {
                "url": url,
                "encode_bank_name": encode_bank_name,
                "bank_idx": bank_idx
            }
            self.logger.info(f"access bank {bank_name} {bank_with_idx} {url}")
            list_url.append(sample)
        self.logger.info(f"all link of banks {len(list_url)}")
        batch_url = []
        for sample in list_url:
            if len(batch_url) > 5:
                with ThreadPoolExecutor(max_workers=5) as executor:
                    try:
                        for _ in tqdm(executor.map(self.extract_link, batch_url), total=len(batch_url)):
                            pass
                    except TimeoutError as e:
                        print("Waited to long", e)
                batch_url = []
                time.sleep(2*len(batch_url))
            else:
                batch_url.append(sample)

    def extract_link(self, sample):
        url = sample['url']
        encode_bank_name = sample['encode_bank_name']
        bank_idx = sample['bank_idx']
        soup = self.browser.access_website(url, True)
        province = soup.find("select", class_="select_province sl_chosen chosen_js")
        if province is None:
            return
        province = province.find_all("option")
        for pro in province:
            province_idx = pro.get("value")
            province_name = pro.text

            encode_province_name = "-".join([unidecode(name.lower()) for name in province_name.split()])

            province_with_idx = "-".join([encode_bank_name, encode_province_name, bank_idx, province_idx])
            url = self.base_url + province_with_idx + ".html"
            self.link_queue.append(url)
            self.logger.info(f"Put {url} into list_url")

    def run(self):
        with open(r"domains/bank/file_link_queue_atm.txt", "r", encoding="utf8") as f:
            self.link_queue = f.readline()
            self.link_queue = self.link_queue.split("\\n")[1:]
        self.logger.info(f"length {len(self.link_queue)}")
        with open(self.save_file, "a", encoding="utf8") as f:
            count = 0
            for url in self.link_queue:
                self.logger.info(f"Get link for parsing {url}")
                max_retries = 2
                while max_retries > 0:
                    try:
                        self.container.driver.get(url=url)
                        district = self.container.driver.find_elements(
                            # By.XPATH, value="//*[@id=\"template_data_province_content\"]/a"
                            By.XPATH, value="//*[@id=\"template_data_company\"]/a"
                        )
                        for dis in district:
                            url = dis.get_attribute("href")
                            if not self.is_crawled(url):
                                if url is not None:
                                    f.write(url + '\n')
                                    self.message_queue.urls.put(url)
                                    count += 1
                                    self.logger.info(f"Total urls {count}")
                                else:
                                    self.logger.info("Found empty url ! Please check your code again")
                        sleep(1)
                        break
                    except Exception as e:
                        self.logger.error(f"Failed to get: {url} | Error {e} | max retries: {max_retries}")
                        max_retries -= 1
                        self.container.force_init()


if __name__ == "__main__":
    import logging

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s | %(name)s | [%(levelname)s] | %(message)s",
        handlers=[
            logging.StreamHandler()
        ]
    )

    from core.storage import Storage
    from domains.vinmec.container import DriverContainer
    from selenium.webdriver.chrome.options import Options as ChromeOptions

    # base_url = "https://thebank.vn/cong-cu/tim-chi-nhanh-ngan-hang/"
    base_url = "https://thebank.vn/cong-cu/tim-atm/"
    save_file = r"storages/bank/thebank_atm.jsonl"
    link_save_file = r"storages/bank/thebank_atm.txt"
    options = ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--igcognito")
    options.add_argument("--disable-extensions")
    link_generator = LinkGenerator(
        base_url=base_url,
        container=DriverContainer(options),
        storage=Storage(),
        save_file=link_save_file
    )
    link_generator.run()
