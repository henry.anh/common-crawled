import json

from typing import Dict
from core.base import BaseContentSaver


class ContentSaver(BaseContentSaver):
    def __init__(self, save_file: str, *, force_new: bool = False) -> None:
        super().__init__()
        assert save_file.endswith(".jsonl"), print("Extension is not supported. Accept *.jsonl only")
        mode = "a" if not force_new else "w"
        self.save_obj = open(save_file, mode=mode, encoding="utf8")

    def run(self, content: Dict[str, str]) -> None:
        try:
            payload = json.dumps(content, ensure_ascii=False)
            self.save_obj.write(payload + "\n")
        except TypeError:
            self.logger.info(f"Failed to save content: {content}")
