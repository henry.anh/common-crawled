from typing import Optional

from selenium.webdriver.common.options import ArgOptions
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver import Chrome
from core.base import BaseDriverContainer
from selenium.webdriver import ChromeService

from utils.fake_module import ScrapeOpsFakeUserAgentMiddleware


class DriverContainer(BaseDriverContainer):
    def __init__(self, options: ArgOptions,
                 fake_agents: Optional[ScrapeOpsFakeUserAgentMiddleware] = None,
                 max_url_accessed: int = 100) -> None:
        super().__init__(options, fake_agents=fake_agents, max_accessed_urls=max_url_accessed)
        self.fake_agents = fake_agents

    def init_driver(self) -> WebDriver:
        user_agent = self.fake_agents.get_fake_agent()
        self.options.add_argument(f'--user-agent={user_agent}')
        service = ChromeService()
        return Chrome(service=service, options=self.options)
